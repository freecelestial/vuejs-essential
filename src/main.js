// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
// 引入根組件，無副檔名
import App from './App'
// 引入路由
import router from './router'
// 引入全局驗證指令
import './directives'
// 引入組件 index.js
import './components'
// 引入 store/index.js 的默认值
import store from './store'
// 引入 plugin 插件
import VueSweetalert2 from './plugins/vue-sweetalert2'
import Message from './plugins/message'

// 引入過濾器
import './filters'
// 引入mock.js 產生測試資料
import { mockArticles } from './mock/data'
import ls from './utils/localStorage'
// 运行 ./mock/index.js
import './mock'
// 引入 axios 的默认值
import axios from 'axios'

// 将 axios 添加到 Vue.prototype 上，使其在实例内部的所有组件中可用
Vue.prototype.$axios = axios



// 使用插件
Vue.use(VueSweetalert2)
Vue.use(Message)


Vue.config.productionTip = true

// 每次使用都直接載入儲存在 localstorage 的數據 加上 60 筆 mock 產生的數據
const AddMockData = (() => {
    // 是否加入测试数据
    const isAddMockData = true
    // 用户数据
    let userArticles = ls.getItem('articles')
  
    if (Array.isArray(userArticles)) {
        userArticles = userArticles.filter(article => parseInt(article.uid) === 1)
    } else {
        userArticles = []
    }
  
    if (isAddMockData) {
        // 合并用户数据和测试数据，使用合并值作为所有文章
        store.commit('UPDATE_ARTICLES', [...userArticles, ...mockArticles(60)])
    } else {
        // 使用用户数据作为所有文章
        store.commit('UPDATE_ARTICLES', userArticles)
    }
})()





// eslint 配置，允许 new 一个实例后不赋值，我们没有使用 eslint，如果有，则下一行注释不可缺少
/* eslint-disable no-new */
// 创建一个新的 Vue 实例
// 內含物件配置選項
new Vue({
    el: '#app',
    // 注入路由
    router,
    // 注入 store
    store,
    // 注入全域元件
    components: { App },
    template: '<App/>'
})
